# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Resoption.create([
    { descripcion: 'OK', abrev: 'OK', activo: 'S', definit: 'S', sql_ccresopt_codigo: '1' },
    { descripcion: 'CAMBIOS DE FILTROS', abrev: 'FILTROS', activo: 'S', definit: 'S', sql_ccresopt_codigo: '2' },
    { descripcion: 'VOLVER', abrev: 'VOLVER', activo: 'S', definit: 'N', sql_ccresopt_codigo: '11' },
    { descripcion: 'ANULADA', abrev: 'ANUL', activo: 'S', definit: 'S', sql_ccresopt_codigo: '12' },
    { descripcion: 'RE-COORDINAR', abrev: 'RE-COORD', activo: 'S', definit: 'S', sql_ccresopt_codigo: '13' },
  ])
