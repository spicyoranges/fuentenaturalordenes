class AgregarDatosParaUsuarios < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :nombre, :string
    add_column :users, :ncompleto, :string
    add_column :users, :telefono, :integer
    add_column :users, :sql_codigo, :integer
  end
end
