class AddAttachmentFotoToReparacions < ActiveRecord::Migration
  def self.up
    change_table :reparacions do |t|
      t.attachment :foto
    end
  end

  def self.down
    remove_attachment :reparacions, :foto
  end
end
