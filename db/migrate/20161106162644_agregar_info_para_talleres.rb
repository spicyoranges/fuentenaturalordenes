class AgregarInfoParaTalleres < ActiveRecord::Migration[5.0]
  def change
    add_column :tallers, :descripcion, :text
    add_column :tallers, :abrev, :string
    add_column :tallers, :observaciones, :text
    add_column :tallers, :user_id, :integer
    add_column :tallers, :sql_cctaller_codigo, :integer
    add_column :tallers, :sql_usuario_codigo, :integer
  end
end
