class AgregarInfoParaReparaciones < ActiveRecord::Migration[5.0]
  def change
    add_column :reparacions, :sector, :integer
    add_column :reparacions, :equipo, :integer
    add_column :reparacions, :observaciones, :text
    add_column :reparacions, :obs_reparacion, :text
    add_column :reparacions, :resolucion, :boolean

    add_column :reparacions, :taller_id, :integer
    add_column :reparacions, :user_id, :integer

    add_column :reparacions, :sql_ccrepara_norden, :integer
  end
end
