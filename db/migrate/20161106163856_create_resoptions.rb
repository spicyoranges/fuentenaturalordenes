class CreateResoptions < ActiveRecord::Migration[5.0]
  def change
    create_table :resoptions do |t|
      t.string :descripcion
      t.string :abrev
      t.boolean :activo
      t.string :definit

      t.integer :sql_ccresopt_codigo

      t.timestamps
    end
  end
end
