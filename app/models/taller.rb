class Taller < ApplicationRecord
  # we can specify prefix, no need when using with 'dbo.' 
  self.table_name_prefix = 'dbo.' 
  # specify table name if it breaks Rails conventions
  self.table_name  = 'CCTALLER'
  # specify primary key name if it's other than id
  self.primary_key = 'ID'

  belongs_to :user
  has_many :reparacions
end
