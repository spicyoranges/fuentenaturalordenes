class User < ApplicationRecord
  # we can specify prefix, no need when using with 'dbo.' 
  self.table_name_prefix = 'dbo.' 
  # specify table name if it breaks Rails conventions
  self.table_name  = 'USUARIOS'
  # specify primary key name if it's other than id
  #self.primary_key = 'ID'

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable,
         :rememberable,
         :trackable,
         :validatable,
         :registerable

  has_many :reparacions
  has_many :tallers
end
