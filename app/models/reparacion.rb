class Reparacion < ApplicationRecord
  # we can specify prefix, no need when using with 'dbo.' 
  self.table_name_prefix = 'dbo.' 
  # specify table name if it breaks Rails conventions
  self.table_name  = 'CCREPARA'
  # specify primary key name if it's other than id
  #self.primary_key = 'ID'

  belongs_to :taller
  belongs_to :user

  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/
  validates :foto, attachment_presence: true

end
