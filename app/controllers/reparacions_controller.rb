class ReparacionsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_reparacion, only: [:show, :edit, :update, :destroy]


  # GET /reparacions
  # GET /reparacions.json
  def index
    @reparacions = current_user.reparacions
  end

  # GET /reparacions/1
  # GET /reparacions/1.json
  def show
  end

  # GET /reparacions/new
  def new
    @reparacion = Reparacion.new
  end

  # GET /reparacions/1/edit
  def edit
  end

  # POST /reparacions
  # POST /reparacions.json
  def create
    @reparacion = Reparacion.new(reparacion_params)

    respond_to do |format|
      if @reparacion.save
        format.html { redirect_to @reparacion, notice: 'Reparacion was successfully created.' }
        format.json { render :show, status: :created, location: @reparacion }
      else
        format.html { render :new }
        format.json { render json: @reparacion.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reparacions/1
  # PATCH/PUT /reparacions/1.json
  def update
    respond_to do |format|
      if @reparacion.update(reparacion_params)
        format.html { redirect_to @reparacion, notice: 'reparacion was successfully updated.' }
        format.json { render :show, status: :ok, location: @reparacion }
      else
        format.html { render :edit }
        format.json { render json: @reparacion.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reparacions/1
  # DELETE /reparacions/1.json
  def destroy
    @reparacion.destroy
    respond_to do |format|
      format.html { redirect_to reparacions_url, notice: 'reparacion was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

private
  # Use callbacks to share common setup or constraints between actions.
  def set_reparacion
    @reparacion = Reparacion.find(params[:id])
  end

  def reparacion_params
    accessible = [ :sector, :equipo, :observaciones, :obs_reparacion, :resolucion, :taller_id, :user_id, :sql_ccrepara_norden ]
    # Agregado para Paperclip
    accessible << [ :foto ]
    #accessible << [ :password, :password_confirmation ] unless params[:user][:password].blank?
    params.require(:reparacion).permit(accessible)
  end
end
