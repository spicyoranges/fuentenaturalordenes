Rails.application.routes.draw do

  devise_for :users
  root 'reparacions#index'

  get 'tecnicos/perfil', as: 'user_root'

  resources :ordenes, only: [:index, :show]

  resources :reparacions
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
